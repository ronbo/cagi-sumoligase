# README #

* Quick summary

These are the documents for an analysis of Ube2I - a SUMO ligase
for the 2015 CAGI competition:

https://genomeinterpretation.org/content/4-SUMO_ligase
Entrez Gene ID: 7329
UniprotKB: http://www.uniprot.org/uniprot/P63279
PDB ID of Ube2I crystal structure: 1A3S
PDB IDs of Ube2I complex co-crystal structures: 1KPS, 2GRN, 2O25, 2PE6, 2UYZ, 2VRR, 3UIN, 3UIO, 3UIP, 4Y1L
* Versions will be fluid as this is a 2 month project
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

RESOURCES

papers on Sumo and SUMO ligase, in yeast and human
alignments and structures

(1) Here is the link to the yeast SUMO  SMT3 with links to the 3D structure database:
http://www.uniprot.org/uniprot/Q12306
(2) Found the conserved UBC9 UNiProt link in Yeast : http://www.uniprot.org/uniprot/P50623
(3)  Link to  159 protein interactions with the ligase   http://thebiogrid.org/31995

Structure Viewers:
http://www.rcsb.org/pdb/static.do?p=software/software_links/molecular_graphics.html

Consurf report on conservation of UBC9 (PDB ID 1a3s):
http://consurf.tau.ac.il/results/1444351607/output.php#finish


## some methods
Several analyses were used to create numerical estimates of folding sensitivity and mutation function likelihood:
1) a conservation score from multiple alignment was created from a multiple alignment using Nucleotide BLAST: 
  http://blast.ncbi.nlm.nih.gov/Blast.cgi

2) A python script that compiled UBC9 contacts in co-crystal structures provided.
These seven profiles were summed to create a per amino acid score for weighted contact with biological binding partners.

3) Surface calculation on the Get Area Server was performed. The ratio 
http://curie.utmb.edu/getarea.html

4) pph2 polyphen2 output for UBC9 our mutants

A linear model of these scores was created that produced a similar distribution by eye to the distributed scores.
Once this was satisfactory, the score minimum was truncated to 0.



### Contribution guidelines ###
Not all the team members had a genomeinterpretation account:
Mike Bowles, Angela Courtney, Ian Driver, Eric Kofman, Clinton Mielke, Ron Shigeta, and Milo Toor

### Who do I talk to? ###

* Repo owner or admin : rtshigeta@yahoo.com
