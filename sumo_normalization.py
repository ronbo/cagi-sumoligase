CONSERVATION_WEIGHT = 1
CONTACT_WEIGHT = .8
#INSIDE_OUTSIDE_WEIGHT = .1
MUTANT_DATA_WEIGHT = 1.2
DSCORE_WEIGHT = .5

import csv 
import numpy
import argparse

def combine_scores(scores):
	"""combine scores using the above coefficients"""
	mutant_scores = scores.get('mutant_data_scores')
	for i,score in enumerate(mutant_scores):
		mutant_scores = score if score != numpy.nan else 0

	conservation_scores = scores.get('conservation_scores')
	for i,score in enumerate(conservation_scores):
		conservation_scores = score if score != numpy.nan else 0

	contact_scores = scores.get('contact_scores')
	for i,score in enumerate(contact_scores):
		contact_scores = score if score != numpy.nan else 0

	dscores = scores.get('dscores')

	combined_scores = CONSERVATION_WEIGHT * conservation_scores
	combined_scores += CONTACT_WEIGHT * -contact_scores + 1
	#combined_scores += INSIDE_OUTSIDE_WEIGHT * scores.get('inside_outside_scores')
	combined_scores += MUTANT_DATA_WEIGHT * mutant_scores
	combined_scores += DSCORE_WEIGHT * dscores
	return combined_scores

def normalize_scores(np_array):
	"""normalize"""
	for i, val in enumerate(np_array):
		if val == None:
			np_array[i] = numpy.nan

	np_array = np_array / np_array.max(axis=0)
	return np_array

def get_scores(csv_filename):
	conservation_scores = []
	contact_scores = []
	inside_outside_scores = []
	mutant_data_scores = []
	dscores = []
	with open(csv_filename, 'rb') as csvfile:
		scoresreader = csv.reader(csvfile, delimiter=',')
		scoresreader.next()
		scoresreader.next()
		for row in scoresreader:
			if '#' in row[0]:
				break
			mutant_data_score = float(row[1]) if row[1] not in ['', '-'] else None
			conservation_score = float(row[3]) if row[3] not in ['', '-'] else None
			inside_outside_score = float(row[4]) if row[4] not in ['', '-'] else None
			contact_score = float(row[6]) if row[6] not in ['', '-'] else None
			dscore = float(row[17]) if row[17] not in ['', '-'] else None

			mutant_data_scores.append(mutant_data_score)
			conservation_scores.append(conservation_score)
			inside_outside_scores.append(inside_outside_score)
			contact_scores.append(contact_score)
			dscores.append(dscore)
	scores = {'conservation_scores': normalize_scores(numpy.array(conservation_scores)),
			  'contact_scores': normalize_scores(numpy.array(contact_scores)),
			  'inside_outside_scores': normalize_scores(numpy.array(inside_outside_scores)),
			  'mutant_data_scores': normalize_scores(numpy.array(mutant_data_scores)),
			  'dscores': normalize_scores(numpy.array(dscores))}
	return scores

def main():
	parser = argparse.ArgumentParser(description='Process some integers.')
	parser.add_argument('file', metavar='filename')

	args = parser.parse_args()

	filename = args.file
	scores = get_scores(filename)

	combined_scores = combine_scores(scores)

	for score in combined_scores:
		print score

if __name__ == '__main__':
	main()