import pandas as pd


path_to_dataset = '4-SUMO_ligase_dataset.txt'
path_to_conserf ='msa_aa_variety_percentage1.csv'

conv_df = pd.DataFrame.from_csv(path_to_conserf)
all_data_dict = {}
all_data_dict['dataset1'] = []
all_data_dict['dataset2'] = []
all_data_dict['dataset3'] = []
with open(path_to_dataset, 'rwb') as f:
	count = 0
	for line in f:
		if line[0] =='#':
			count += 1
		if count == 1 and line[0] != '#':
			all_data_dict['dataset1'].append(line.strip())
		if count == 2 and line[0] != '#':
			all_data_dict['dataset2'].append(line.strip().split(','))
		if count == 3 and line[0] != '#':
			all_data_dict['dataset3'].append(line.strip().split(','))
data_df = pd.DataFrame(dict([(k,pd.Series(v)) for k,v in all_data_dict.iteritems() ]))


def classify_mut(per_mut):
	if per_mut >= 20:
		return 1
	elif per_mut >=0.01 and per_mut < 20:
		return 0.5
	else:
		return 0

conv_by_pos = conv_df.transpose()
for mut in all_data_dict['dataset1']:
	pos = mut[1:-1]
	pos_aa_df = conv_by_pos[int(pos)+2]
	pos_aa_df_t = pos_aa_df.transpose()
	print "{}: {}".format(mut, classify_mut(pos_aa_df_t[mut[-1]]))






